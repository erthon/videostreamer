#ifndef DATAFRAME_CPP
#define DATAFRAME_CPP

#include "include/DataFrame.h"

	DataFrame::DataFrame(uint8_t* NewDataBuffer, uint32_t NewFrameWidth, uint32_t NewFrameHeight) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewFrameWidth * NewFrameHeight * 4), IsVideoData(true), FrameWidth(NewFrameWidth), FrameHeight(NewFrameHeight), IsAudioData(false), AudioChannelCount(0), AudioSampleRate(0)
	{
		VideoDataBuffer = new uint8_t[NewFrameWidth * NewFrameHeight * 4];
		uint32_t i = 0;
		for (uint32_t y = 0; y <  NewFrameHeight; y++)
		{
			for (uint32_t x = 0; x < NewFrameWidth; x++)
			{
				*(VideoDataBuffer + i) = *(NewDataBuffer + i);
				*(VideoDataBuffer + i + 1) = *(NewDataBuffer + i + 1);
				*(VideoDataBuffer + i + 2) = *(NewDataBuffer + i + 2);
				*(VideoDataBuffer + i + 3) = *(NewDataBuffer + i + 3);
				i += 4;
			}
		}
	};
	DataFrame::DataFrame(uint32_t NewFrameWidth, uint32_t NewFrameHeight) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewFrameWidth * NewFrameHeight * 4), IsVideoData(true), FrameWidth(NewFrameWidth), FrameHeight(NewFrameHeight), IsAudioData(false), AudioChannelCount(0), AudioSampleRate(0)
	{
		VideoDataBuffer = new uint8_t[NewFrameWidth * NewFrameHeight * 4];
		uint32_t i = 0;
		for (uint32_t y = 0; y <  NewFrameHeight; y++)
		{
			for (uint32_t x = 0; x < NewFrameWidth; x++)
			{
				*(VideoDataBuffer + i) = 0;
				*(VideoDataBuffer + i + 1) = 0;
				*(VideoDataBuffer + i + 2) = 0;
				*(VideoDataBuffer + i + 3) = 255;
				i += 4;
			}
		}
	};
	DataFrame::DataFrame(uint8_t* NewAudioBuffer, uint32_t NewSampleCount, uint32_t NewAudioChannelCount, uint32_t NewAudioSampleRate) : VideoDataBuffer(nullptr), AudioDataBuffer(nullptr), BufferSize(NewSampleCount * NewAudioChannelCount), IsVideoData(false), FrameWidth(0), FrameHeight(0), IsAudioData(true), AudioChannelCount(NewAudioChannelCount), AudioSampleRate(NewAudioSampleRate)
	{
		AudioDataBuffer = new int16_t[NewSampleCount * NewAudioChannelCount];
		int16_t* sourcearray = (int16_t*)NewAudioBuffer;
		for (int i = 0; i < (int)(NewSampleCount * NewAudioChannelCount); i++)
		{
			*(AudioDataBuffer + i) = *(sourcearray + i);
		}
	};
	DataFrame::~DataFrame()
	{
		delete[] VideoDataBuffer;
		delete[] AudioDataBuffer;
	};

#endif