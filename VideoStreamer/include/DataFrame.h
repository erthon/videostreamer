#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

#include <stdint.h>

struct DataFrame
{
public:
	uint8_t* VideoDataBuffer; // RGBA Pixel Buffer
	int16_t* AudioDataBuffer; // Signed 16 Byte (PCM) Audio Buffer
	uint32_t BufferSize; // Length of the Video or Audio buffer
	bool IsVideoData; // Flag if this frame is Video data
	uint32_t FrameWidth; // Video width
	uint32_t FrameHeight; // Video height
	bool IsAudioData; // Flag if this frame is Audio data
	uint32_t AudioChannelCount; // Audio channel count
	uint32_t AudioSampleRate; // Audio sample count

public:
	DataFrame(uint8_t* NewDataBuffer, uint32_t NewFrameWidth, uint32_t NewFrameHeight); // Constructor for a Video frame
	DataFrame(uint32_t NewFrameWidth, uint32_t NewFrameHeight); // Contructor for a solid black Video frame
	DataFrame(uint8_t* NewAudioBuffer, uint32_t NewSampleCount, uint32_t NewAudioChannelCount, uint32_t NewAudioSampleRate); // Contructor for an Audio frame
	~DataFrame(); // Destructor
};
#endif