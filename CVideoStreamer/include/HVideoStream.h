#ifndef HVIDEOSTREAM_H
#define HVIDEOSTREAM_H

#define exported __declspec(dllexport)

#include "include/VideoStream.h"
#include "include/HDataFrame.h"

extern "C"
{
	struct HVideoStream;
	typedef struct HVideoStream HVideoStream;
	exported HVideoStream* CreateVideoStream(const char* FilePath, int32_t* ReturnValue, bool ForceOptions, bool EnableVideo, bool EnableAudio);
	exported void DeleteVideoStream(HVideoStream* CurrentVideoStream);
	exported void ResettoBegining(HVideoStream* CurrentVideoStream);
	exported void SetPlayingOffset(HVideoStream* CurrentVideoStream, int64_t MSPosition);
	exported HDataFrame* DecodeNextDataFrame(HVideoStream* CurrentVideoStream);
	exported float GetVideoFPMS(HVideoStream* CurrentVideoStream);
	exported bool GetEndOfFile(HVideoStream* CurrentVideoStream);
	exported bool GetHasVideo(HVideoStream* CurrentVideoStream);
	exported bool GetHasAudio(HVideoStream* CurrentVideoStream);
	exported int64_t GetDuration(HVideoStream* CurrentVideoStream);
	exported uint32_t GetVideoHeight(HVideoStream* CurrentVideoStream);
	exported uint32_t GetVideoWidth(HVideoStream* CurrentVideoStream);
	exported uint32_t GetAudioChannelCount(HVideoStream* CurrentVideoStream);
	exported uint32_t GetAudioSampleRate(HVideoStream* CurrentVideoStream);
}

#endif