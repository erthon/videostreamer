﻿using System;
using System.Security;
using System.Runtime.InteropServices;

namespace VideoStreamerNET
{
    internal static class Imports
    {
        internal const string VideoStreamerDLL = "CVideoStreamer";
        //Create/Delete

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern unsafe IntPtr CreateVideoStream(string FilePath, Int32* ReturnValue, [MarshalAs(UnmanagedType.I1)]bool ForceOptions, [MarshalAs(UnmanagedType.I1)]bool EnableVideo, [MarshalAs(UnmanagedType.I1)]bool EnableAudio);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern void DeleteVideoStream(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern IntPtr CreateVideoFrame(UInt32 FrameWidth, UInt32 FrameHeight);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern void DeleteDataFrame(IntPtr DataFrameHandle);

        //VideoStream Functions

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern void ResettoBegining(IntPtr VideoStreamHandle);

         [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern void SetPlayingOffset(IntPtr VideoStreamHandle, Int64 MSPosition);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern IntPtr DecodeNextDataFrame(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern Single GetVideoFPMS(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        [return: MarshalAsAttribute(UnmanagedType.I1)]
        internal static extern bool GetEndOfFile(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        [return: MarshalAsAttribute(UnmanagedType.I1)]
        internal static extern bool GetHasVideo(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        [return: MarshalAsAttribute(UnmanagedType.I1)]
        internal static extern bool GetHasAudio(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern Int64 GetDuration(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetVideoHeight(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetVideoWidth(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetAudioChannelCount(IntPtr VideoStreamHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetAudioSampleRate(IntPtr VideoStreamHandle);

        //VideoFrame Functions

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern unsafe byte* GetVideoDataBuffer(IntPtr VideoFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern unsafe Int16* GetAudioDataBuffer(IntPtr AudioFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        [return: MarshalAsAttribute(UnmanagedType.I1)]
        internal static extern bool GetIsVideoData(IntPtr DataFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        [return: MarshalAsAttribute(UnmanagedType.I1)]
        internal static extern bool GetIsAudioData(IntPtr DataFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetBufferSize(IntPtr DataFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetFrameWidth(IntPtr VideoFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetFrameHeight(IntPtr VideoFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetFrameChannelCount(IntPtr AudioFrameHandle);

        [DllImport(VideoStreamerDLL, CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        internal static extern UInt32 GetFrameSampleRate(IntPtr AudioFrameHandle);
    }
}
