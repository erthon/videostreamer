﻿using System;
using System.Runtime.InteropServices;

namespace VideoStreamerNET
{
    public class VideoBuffer
    {
        #region Varibles
        private IntPtr _handle = IntPtr.Zero;
        #endregion

        #region Properties
        public byte this[UInt32 index]
        {
            get
            {
                unsafe
                {
                    byte* basearray = Imports.GetVideoDataBuffer(_handle);
                    return *(basearray + index);
                }
            }
        }
        public UInt32 Length
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal VideoBuffer(IntPtr VideoFrameHandle)
        {
            _handle = VideoFrameHandle;
        }
        #endregion

        #region Functions
        public byte[] ToArray()
        {
            byte[] managedbytearray = new byte[Length];
            unsafe
            {
                Marshal.Copy(new IntPtr(Imports.GetVideoDataBuffer(_handle)), managedbytearray, 0, (int)Length);
            }
            return managedbytearray;
        }
        #endregion
    }
}
