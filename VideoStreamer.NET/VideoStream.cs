﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using SFML.Window;
using SFML.Graphics;

namespace VideoStreamerNET
{
    public enum PlayState
    {
        Stopped = 1,
        Playing = 2,
        Paused = 3
    }
    public class VideoStream : IDisposable
    {
        #region Varibles
        private IntPtr _handle = IntPtr.Zero;
        private PlayState _state = PlayState.Stopped;
        private Thread _decodethread = null;
        private bool _shouldthreadrun = true;
        private bool _isthreadrunning = false;
        private bool _isdecoding = false;
        private bool _isplayingtoeof = false;
        private bool _iseof = false;
        private bool _hasvideo = false;
        private bool _hasaudio = false;
        private uint _videoframequeueamount = 10;
        private uint _audioframequeueamount = 10;
        private Vector2u _videosize = new Vector2u(1, 1);
        private ConcurrentQueue<VideoFrame> _queuedvideoframes = null;
        private ConcurrentQueue<AudioFrame> _queuedaudioframes = null;
        private AudioStream _currentaudiostream = null;
        private VideoFrame _currentvideoframe = null;
        private Texture _currenttexture = null;
        private Single _videoframejumptime = 1;
        private double _videoelapsedtime = 0;
        private long _totalelapsedtime = 0;
        private int _jumpvideoframecount = 0;
        private bool _disposed = false;
        #endregion

        #region Properties
        public bool CanPlayVideo
        {
            get
            {
                return _hasvideo;
            }
        }
        public bool CanPlayAudio
        {
            get
            {
                return _hasaudio;
            }
        }
        public Texture DisplayVideoFrameTexture
        {
            get
            {
                return _currenttexture;
            }
        }
        public VideoFrame DisplayVideoFrameData
        {
            get
            {
                return _currentvideoframe;
            }
        }
        public PlayState CurrentState
        {
            get
            {
                return _state;
            }
        }
        public long VideoLengthMS
        {
            get
            {
                return Imports.GetDuration(_handle) / 1000;
            }
        }
        public float VideoLengthSeconds
        {
            get
            {
                return (float)VideoLengthMS / 1000.0f;
            }
        }
        public float VideoLengthMinutes
        {
            get
            {
                return VideoLengthSeconds / 60.0f;
            }
        }
        public long VideoMSPosition
        {
            get
            {
                return _totalelapsedtime;
            }
            set
            {
                bool startplaying = (_state == PlayState.Playing);
                Stop();
                _totalelapsedtime = value;
                Imports.SetPlayingOffset(_handle, value);
                if (startplaying) Play();
            }
        }
        public float VideoSecondsPosition
        {
            get
            {
                return (float)VideoMSPosition / 1000.0f;
            }
            set
            {
                VideoMSPosition = (long)Math.Round(value * 1000.0f, 0);
            }
        }
        public float VideoMinutesPosition
        {
            get
            {
                return (float)VideoSecondsPosition / 60.0f;
            }
            set
            {
                VideoSecondsPosition = value * 60.0f;
            }
        }
        #endregion

        #region Events
        public event EventHandler EndofFileReached;
        #endregion

        #region Constructors/Destructors
        public VideoStream(string FilePath) : this(FilePath, 10, 10, true, 350, true, true, true) { }
        public VideoStream(string FilePath, uint VideoFrameQueueAmount, uint AudioFrameQueueAmount, bool UseInternalVideoTexture, uint AudioSyncTime, bool ForceOptions, bool EnableVideo, bool EnableAudio)
        {
            Int32 returnvalue = 0;
            unsafe
            {
                _handle = Imports.CreateVideoStream(FilePath, &returnvalue, ForceOptions, EnableVideo, EnableAudio);
            }
            if (returnvalue != 0)
            {
                Imports.DeleteVideoStream(_handle);
                _handle = IntPtr.Zero;
                HandleCreateError(returnvalue);
            }
            else
            {
                _hasvideo = Imports.GetHasVideo(_handle);
                _hasaudio = Imports.GetHasAudio(_handle);
                if (_hasvideo)
                {
                    _videosize = new Vector2u(Imports.GetVideoWidth(_handle), Imports.GetVideoHeight(_handle));
                    _queuedvideoframes = new ConcurrentQueue<VideoFrame>();
                    _videoframejumptime = Imports.GetVideoFPMS(_handle);
                    _videoframequeueamount = VideoFrameQueueAmount;
                    _currentvideoframe = new VideoFrame(Imports.CreateVideoFrame(_videosize.X, _videosize.Y));
                    if (UseInternalVideoTexture)
                    {
                        _currenttexture = new Texture(_videosize.X, _videosize.Y);
                        _currenttexture.Smooth = true;
                        _currenttexture.Update(_currentvideoframe.VideoDataBuffer.ToArray());
                    }
                }
                if (_hasaudio)
                {
                    _queuedaudioframes = new ConcurrentQueue<AudioFrame>();
                    _currentaudiostream = new AudioStream(Imports.GetAudioChannelCount(_handle), Imports.GetAudioSampleRate(_handle), _queuedaudioframes, (int)AudioSyncTime);
                    _audioframequeueamount = AudioFrameQueueAmount;
                }
                _decodethread = new Thread(new ThreadStart(DecodeThreadEntry));
                _decodethread.Start();
            }
        }
        ~VideoStream()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _shouldthreadrun = false;
                _decodethread.Join();
                Imports.DeleteVideoStream(_handle);
                _currentvideoframe.Dispose();
                _currentvideoframe = null;
                while (_queuedvideoframes.Count > 0)
                {
                    VideoFrame frame = null;
                    if (_queuedvideoframes.TryDequeue(out frame))
                    {
                        frame.Dispose();
                        frame = null;
                    }
                }
                while (_queuedaudioframes.Count > 0)
                {
                    AudioFrame frame = null;
                    if (_queuedaudioframes.TryDequeue(out frame))
                    {
                        frame.Dispose();
                        frame = null;
                    }
                }
            }
        }
        #endregion

        #region Decode Thread
        private void DecodeThreadEntry()
        {
            _isthreadrunning = true;
            while (_shouldthreadrun)
            {
                if (_isdecoding)
                {
                    bool queuevideo = false;
                    bool queueaudio = false;
                    if (_hasvideo) queuevideo = (_queuedvideoframes.Count < _videoframequeueamount);
                    if (_hasaudio) queueaudio = (_queuedaudioframes.Count < _audioframequeueamount);
                    if (queuevideo || queueaudio)
                    {
                        IntPtr newframehandle = Imports.DecodeNextDataFrame(_handle);
                        if (newframehandle != IntPtr.Zero)
                        {
                            // got frame
                            // check to determine if it is audio or video
                            bool isvideo = Imports.GetIsVideoData(newframehandle);
                            bool isaudio = Imports.GetIsAudioData(newframehandle);
                            if (isvideo && _hasvideo)
                            {
                                VideoFrame newframe = new VideoFrame(newframehandle);
                                _queuedvideoframes.Enqueue(newframe);
                            }
                            else if (isaudio && _hasaudio)
                            {
                                AudioFrame newframe = new AudioFrame(newframehandle);
                                _queuedaudioframes.Enqueue(newframe);
                            }
                            else
                            {
                                Imports.DeleteDataFrame(newframehandle);
                            }
                        }
                        else
                        {
                            // no frame returned
                            // check for eof
                            if (Imports.GetEndOfFile(_handle))
                            {
                                _isdecoding = false;
                                Imports.ResettoBegining(_handle);
                                _isplayingtoeof = true;
                                if (_hasaudio) _currentaudiostream.PlayingtoEof = true;
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(5);
                    }
                }
                else
                {
                    Thread.Sleep(5);
                }
            }
            _isthreadrunning = false;
        }
        #endregion

        #region Functions
        public void UpdateFrameTime(long ElapsedMSTime)
        {
            if (_state == PlayState.Playing)
            {
                _totalelapsedtime += ElapsedMSTime;
                if (_hasvideo) _videoelapsedtime += ElapsedMSTime;
                if (_hasaudio) _currentaudiostream.UpdateFrameTime(ElapsedMSTime);
            }
            if (_hasvideo)
            {
                while (_videoelapsedtime > _videoframejumptime)
                {
                    _videoelapsedtime -= _videoframejumptime;
                    _jumpvideoframecount += 1;
                }
                while (_queuedvideoframes.Count > 0 && _jumpvideoframecount > 0)
                {
                    VideoFrame frame = null;
                    if (_queuedvideoframes.TryDequeue(out frame))
                    {
                        if (frame.VideoBufferSize > 0)
                        {
                            _currentvideoframe.Dispose();
                            _currentvideoframe = null;
                            _currentvideoframe = frame;
                            if (_queuedvideoframes.Count == 1 || _jumpvideoframecount == 1)
                            {
                                if (_currenttexture != null)
                                {
                                    _currenttexture.Update(_currentvideoframe.VideoDataBuffer.ToArray());
                                }
                            }
                        }
                        else
                        {
                            frame.Dispose();
                            frame = null;
                        }
                        _jumpvideoframecount -= 1;
                    }
                }
                if (_queuedvideoframes.Count == 0 && _isplayingtoeof && _state == PlayState.Playing)
                {
                    _iseof = true;
                }
            }
            bool videof = true;
            bool audeof = true;
            if (_hasvideo) videof = _iseof;
            if (_hasaudio) audeof = _currentaudiostream.IsEof;
            if (videof && audeof)
            {
                Stop();
                if (EndofFileReached != null) EndofFileReached(this, null);
            }
        }
        public void Play()
        {
            if (_state != PlayState.Playing)
            {
                if (_state == PlayState.Stopped)
                {
                    _isplayingtoeof = false;
                    if (_hasaudio) _currentaudiostream.PlayingtoEof = false;
                }
                if (_hasaudio) _currentaudiostream.Play();
                _state = PlayState.Playing;
                _isdecoding = true;
            }
        }
        public void Pause()
        {
            if (_state == PlayState.Playing)
            {
                _state = PlayState.Paused;
                if (_hasaudio) _currentaudiostream.Pause();
                _isdecoding = false;
            }
        }
        public void Stop()
        {
            if (_state != PlayState.Stopped)
            {
                _state = PlayState.Stopped;
                _isplayingtoeof = false;
                _iseof = false;
                if (_hasaudio) _currentaudiostream.PlayingtoEof = false;
                if (_hasaudio) _currentaudiostream.Stop();
                if (_hasaudio) _currentaudiostream.IsEof = false;
                _isdecoding = false;
                _videoelapsedtime = 0;
                _totalelapsedtime = 0;
                Imports.ResettoBegining(_handle);
                if (_hasaudio) _currentaudiostream.ResettoBegining();
                if (_hasvideo)
                {
                    while (_queuedvideoframes.Count > 0)
                    {
                        VideoFrame frame = null;
                        if (_queuedvideoframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
                if (_hasaudio)
                {
                    while (_queuedaudioframes.Count > 0)
                    {
                        AudioFrame frame = null;
                        if (_queuedaudioframes.TryDequeue(out frame))
                        {
                            frame.Dispose();
                            frame = null;
                        }
                    }
                }
                if (_hasvideo)
                {
                    _currentvideoframe.Dispose();
                    _currentvideoframe = null;
                    _currentvideoframe = new VideoFrame(Imports.CreateVideoFrame(_videosize.X, _videosize.Y));
                    if (_currenttexture != null)
                    {
                        _currenttexture.Update(_currentvideoframe.VideoDataBuffer.ToArray());
                    }
                }
            }
        }
        #endregion

        #region Error Handling
        private void HandleCreateError(int ErrorCode)
        {
            if (ErrorCode == 1)
            {
                throw new Exception("Unable to load specified file");
            }
            else if (ErrorCode == 2)
            {
                throw new Exception("Unable to find the stream information");
            }
            else if (ErrorCode == 3)
            {
                throw new Exception("Unable to find a valid video stream");
            }
            else if (ErrorCode == 4)
            {
                throw new Exception("Unable to find a valid audio stream");
            }
            else if (ErrorCode == 5)
            {
                throw new Exception("Unable to get the video codec context");
            }
            else if (ErrorCode == 6)
            {
                throw new Exception("Unable to find a video codec");
            }
            else if (ErrorCode == 7)
            {
                throw new Exception("Unable to get the audio codec context");
            }
            else if (ErrorCode == 8)
            {
                throw new Exception("Unable to find an audio codec");
            }
            else if (ErrorCode == 9)
            {
                throw new Exception("Unable to load video codec");
            }
            else if (ErrorCode == 10)
            {
                throw new Exception("Unable to load audio codec");
            }
            else if (ErrorCode == 11)
            {
                throw new Exception("Unable to create video buffers");
            }
            else if (ErrorCode == 12)
            {
                throw new Exception("Unable to create audio buffers");
            }
            else if (ErrorCode == 13)
            {
                throw new Exception("Unable to load audio or video");
            }
        }
        #endregion
    }
}
