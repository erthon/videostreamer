﻿using System;

namespace VideoStreamerNET
{
    public class AudioFrame : IDisposable
    {
        #region Varibles
        private IntPtr _handle = IntPtr.Zero;
        private AudioBuffer _currentaudiobuffer = null;
        private bool _disposed = false;
        #endregion

        #region Properties
        public AudioBuffer AudioDataBuffer
        {
            get
            {
                return _currentaudiobuffer;
            }
        }
        public UInt32 AudioBufferSize
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        public UInt32 FrameChannelCount
        {
            get
            {
                return Imports.GetFrameChannelCount(_handle);
            }
        }
        public UInt32 FrameSampleRate
        {
            get
            {
                return Imports.GetFrameSampleRate(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal AudioFrame(IntPtr AudioFrameHandle)
        {
            _handle = AudioFrameHandle;
            _currentaudiobuffer = new AudioBuffer(AudioFrameHandle);
        }
        ~AudioFrame()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _currentaudiobuffer = null;
                Imports.DeleteDataFrame(_handle);
            }
        }
        #endregion
    }
}
