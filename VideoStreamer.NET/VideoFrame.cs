﻿using System;

namespace VideoStreamerNET
{
    public class VideoFrame : IDisposable
    {
        #region Varibles
        private IntPtr _handle = IntPtr.Zero;
        private VideoBuffer _currentvideobuffer = null;
        private bool _disposed = false;
        #endregion

        #region Properties
        public VideoBuffer VideoDataBuffer
        {
            get
            {
                return _currentvideobuffer;
            }
        }
        public UInt32 VideoBufferSize
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        public UInt32 FrameWidth
        {
            get
            {
                return Imports.GetFrameWidth(_handle);
            }
        }
        public UInt32 FrameHeight
        {
            get
            {
                return Imports.GetFrameHeight(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal VideoFrame(IntPtr VideoFrameHandle)
        {
            _handle = VideoFrameHandle;
            _currentvideobuffer = new VideoBuffer(VideoFrameHandle);
        }
        ~VideoFrame()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                _currentvideobuffer = null;
                Imports.DeleteDataFrame(_handle);
            }
        }
        #endregion
    }
}
