﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using SFML.Audio;

namespace VideoStreamerNET
{
    internal class AudioStream : SFML.Audio.SoundStream
    {
        #region Varibles
        private uint _channelcount = 0;
        private Single _audiosamplesperms = 1;
        private double _audiosampleposition = 0;
        private Mutex _timeupdatemutex = null;
        private ConcurrentQueue<AudioFrame> _queuedaudioframes = null;
        private AudioFrame _currentaudioframe = null;
        private bool _isplayingtoeof = false;
        private bool _iseof = false;
        #endregion

        #region Properties
        public bool PlayingtoEof
        {
            get
            {
                return _isplayingtoeof;
            }
            set
            {
                _isplayingtoeof = value;
            }
        }
        public bool IsEof
        {
            get
            {
                return _iseof;
            }
            set
            {
                _iseof = value;
            }
        }
        #endregion

        #region Consts
        private readonly int AudioCorrectionTime = 300;
        #endregion

        #region Contructors/Destructors
        public AudioStream(uint ChannelCount, uint SampleRate, ConcurrentQueue<AudioFrame> CurrentAudioFrameQueue, int NewAudioCorrectionTime)
        {
            AudioCorrectionTime = NewAudioCorrectionTime;
            _channelcount = ChannelCount;
            _audiosamplesperms = (float)SampleRate / 1000.0f;
            _queuedaudioframes = CurrentAudioFrameQueue;
            _timeupdatemutex = new Mutex();
            Initialize(ChannelCount, SampleRate);
        }
        #endregion

        #region Functions
        protected override void OnSeek(TimeSpan timeOffset)
        {
            // not needed
        }
        protected override bool OnGetData(out short[] returnedbuffer)
        {
            while (_queuedaudioframes.Count == 0 && !_isplayingtoeof)
            {
                Thread.Sleep(5);
            }
            if (_queuedaudioframes.Count == 0 && _isplayingtoeof)
            {
                _iseof = true;
                returnedbuffer = new short[0];
                return false;
            }
            _timeupdatemutex.WaitOne();
            if (_currentaudioframe != null) _currentaudioframe.Dispose();
            _currentaudioframe = null;
            bool validbuffer = false;
            short[] finalbuffer = null;
            while (!validbuffer)
            {
                if (_audiosampleposition > AudioCorrectionTime)
                {
                    // skip samples
                    int finalbufferlength = 0;
                    while (finalbufferlength == 0)
                    {
                        if (_currentaudioframe != null) _currentaudioframe.Dispose();
                        _currentaudioframe = null;
                        while (_queuedaudioframes.Count == 0)
                        {
                            _timeupdatemutex.ReleaseMutex();
                            Thread.Sleep(5);
                            if (_queuedaudioframes.Count == 0 && _isplayingtoeof)
                            {
                                _iseof = true;
                                returnedbuffer = new short[0];
                                return false;
                            }
                            _timeupdatemutex.WaitOne();
                        }
                        if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                        {
                            int skipsamplecount = (int)Math.Max((int)Math.Floor(_audiosampleposition * _audiosamplesperms), 0);
                            finalbufferlength = (int)Math.Max(_currentaudioframe.AudioBufferSize - skipsamplecount, 0);
                            _audiosampleposition -= _currentaudioframe.AudioBufferSize / _audiosamplesperms;
                            if (finalbufferlength > 0)
                            {
                                short[] audiodata = _currentaudioframe.AudioDataBuffer.ToArray();
                                finalbuffer = new short[finalbufferlength];
                                Array.Copy(audiodata, Math.Min(skipsamplecount, audiodata.Length), finalbuffer, 0, finalbuffer.Length);
                                validbuffer = true;
                            }
                        }
                    }
                }
                else if (_audiosampleposition < -AudioCorrectionTime)
                {
                    // add samples
                    int extrasamplecount = (int)Math.Floor(-_audiosampleposition * _audiosamplesperms);
                    _audiosampleposition += extrasamplecount / _audiosamplesperms;
                    if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                    {
                        _audiosampleposition -= _currentaudioframe.AudioBufferSize / _audiosamplesperms;
                        short[] audiodata = _currentaudioframe.AudioDataBuffer.ToArray();
                        finalbuffer = new short[extrasamplecount + audiodata.Length];
                        audiodata.CopyTo(finalbuffer, extrasamplecount);
                        validbuffer = true;
                    }
                    else
                    {
                        _currentaudioframe = null;
                        finalbuffer = new short[extrasamplecount];
                        validbuffer = true;
                    }
                }
                else
                {
                    // continue as normal
                    if (_queuedaudioframes.TryDequeue(out _currentaudioframe))
                    {
                        _audiosampleposition -= _currentaudioframe.AudioBufferSize / _audiosamplesperms;
                        finalbuffer = _currentaudioframe.AudioDataBuffer.ToArray();
                        validbuffer = true;
                    }
                }
            }
            _timeupdatemutex.ReleaseMutex();
            returnedbuffer = finalbuffer;
            return true;
        }
        public void UpdateFrameTime(long ElapsedMSTime)
        {
            _timeupdatemutex.WaitOne();
            _audiosampleposition += ElapsedMSTime;
            _timeupdatemutex.ReleaseMutex();
        }
        public void ResettoBegining()
        {
            _timeupdatemutex.WaitOne();
            _audiosampleposition = 0;
            _timeupdatemutex.ReleaseMutex();
        }
        #endregion
    }
}
