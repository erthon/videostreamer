﻿using System;
using System.Runtime.InteropServices;

namespace VideoStreamerNET
{
    public class AudioBuffer
    {
        #region Varibles
        private IntPtr _handle = IntPtr.Zero;
        #endregion

        #region Properties
        public Int16 this[UInt32 index]
        {
            get
            {
                unsafe
                {
                    Int16* basearray = Imports.GetAudioDataBuffer(_handle);
                    return *(basearray + index);
                }
            }
        }
        public UInt32 Length
        {
            get
            {
                return Imports.GetBufferSize(_handle);
            }
        }
        #endregion

        #region Constructors/Destructors
        internal AudioBuffer(IntPtr AudioFrameHandle)
        {
            _handle = AudioFrameHandle;
        }
        #endregion

        #region Functions
        public Int16[] ToArray()
        {
            Int16[] managedbytearray = new Int16[Length];
            unsafe
            {
                Marshal.Copy(new IntPtr(Imports.GetAudioDataBuffer(_handle)), managedbytearray, 0, (int)Length);
            }
            return managedbytearray;
        }
        #endregion
    }
}
