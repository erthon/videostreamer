﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SFML;
using SFML.Graphics;
using SFML.Window;
using System.Diagnostics;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start our program
            new VideoStreamerTest().Run();
        }
    }
    public class VideoStreamerTest
    {
        VideoStreamerNET.VideoStream stream = null;
        public void Run()
        {
            //Create our window
            RenderWindow window = new RenderWindow(new VideoMode(640, 480), "VideoStreamer Test");
            window.SetFramerateLimit(60);
            //Add handlers for some events
            window.MouseButtonPressed += MouseClicked;
            //Create a text object for displaying position/duration
            Font fnt = new Font(@"C:\Windows\Fonts\arial.ttf");
            Text posdisplay = new Text("", fnt);
            posdisplay.CharacterSize = 18;
            posdisplay.Position = new Vector2f(285, 390);
            //Create some buttons
            VertexArray playbutton = new VertexArray(PrimitiveType.Triangles, 3);
            playbutton[0] = new Vertex(new Vector2f(255, 350), Color.Green);
            playbutton[1] = new Vertex(new Vector2f(275, 360), Color.Green);
            playbutton[2] = new Vertex(new Vector2f(255, 370), Color.Green);
            VertexArray pausebutton = new VertexArray(PrimitiveType.Quads, 8);
            pausebutton[0] = new Vertex(new Vector2f(305, 350), Color.Blue);
            pausebutton[1] = new Vertex(new Vector2f(310, 350), Color.Blue);
            pausebutton[2] = new Vertex(new Vector2f(310, 370), Color.Blue);
            pausebutton[3] = new Vertex(new Vector2f(305, 370), Color.Blue);
            pausebutton[4] = new Vertex(new Vector2f(320, 350), Color.Blue);
            pausebutton[5] = new Vertex(new Vector2f(325, 350), Color.Blue);
            pausebutton[6] = new Vertex(new Vector2f(325, 370), Color.Blue);
            pausebutton[7] = new Vertex(new Vector2f(320, 370), Color.Blue);
            VertexArray stopbutton = new VertexArray(PrimitiveType.Quads, 4);
            stopbutton[0] = new Vertex(new Vector2f(355, 350), Color.Red);
            stopbutton[1] = new Vertex(new Vector2f(375, 350), Color.Red);
            stopbutton[2] = new Vertex(new Vector2f(375, 370), Color.Red);
            stopbutton[3] = new Vertex(new Vector2f(355, 370), Color.Red);
            //Create our video stream
            stream = new VideoStreamerNET.VideoStream("VIDEO FILE PATH");
            //Get a timer to track elapsed time
            Stopwatch timer = new Stopwatch();
            timer.Restart();
            //Create a sprite to draw our video frame
            Sprite framesprite = new Sprite(stream.DisplayVideoFrameTexture);
            //Keep looping while our window is open
            while (window.IsOpen())
            {
                //Handle current events
                window.DispatchEvents();
                //Clear current window
                window.Clear();
                //Get our current frame time and restart the timer
                long frametime = timer.ElapsedMilliseconds;
                timer.Restart();
                //Update the current frame inside our video streamer
                stream.UpdateFrameTime(frametime);
                //Update our position/duration display
                posdisplay.DisplayedString = stream.VideoSecondsPosition + " \\ " + stream.VideoLengthSeconds;
                //Draw the current video frame
                window.Draw(framesprite);
                //Draw our buttons
                window.Draw(playbutton);
                window.Draw(pausebutton);
                window.Draw(stopbutton);
                //Draw our position/duration display
                window.Draw(posdisplay);
                //Now display everything on the screen
                window.Display();
            }
        }
        private void MouseClicked(object sender, MouseButtonEventArgs e)
        {
            //Define button areas
            IntRect playarea = new IntRect(255, 350, 20, 20);
            IntRect pausearea = new IntRect(305, 350, 20, 20);
            IntRect stoparea = new IntRect(355, 350, 20, 20);
            //Check button areas and take the appropriate action play/pause/stop
            if (playarea.Contains(e.X, e.Y))
            {
                stream.Play();
            }
            else if (pausearea.Contains(e.X, e.Y))
            {
                stream.Pause();
            }
            else if (stoparea.Contains(e.X, e.Y))
            {
                stream.Stop();
            }
        }
    }

}
